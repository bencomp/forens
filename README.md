# ForeNS

ForeNS is a web service that offers train journeys based on user-set
preferences. Its audience consists of commuters who have make the
same journey regularly, but who do not want to check for changes and
disturbances before every trip.

## Usage

Provide a schedule that contains start and end stations, preferred
time of departure and days of the week and email address to the
service. ForeNS will then try to find your journeys and send an
invitation as iCalendar item to the provided email address. If there
are multiple options for the provided schedule, multiple invitations
may be sent.

Accept the invitation (that best matches your preferred journey, if
there are multiple options) to add it to your calendar and to have
ForeNS keep a watch for changes and disturbances. If any significant
changes occur for your planned trip, ForeNS may send a cancellation
or suggest a new trip.
